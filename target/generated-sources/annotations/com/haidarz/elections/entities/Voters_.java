package com.haidarz.elections.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.4.v20160829-rNA", date="2017-02-21T12:31:28")
@StaticMetamodel(Voters.class)
public class Voters_ { 

    public static volatile SingularAttribute<Voters, String> lastName;
    public static volatile SingularAttribute<Voters, Integer> registrationNb;
    public static volatile SingularAttribute<Voters, String> sex;
    public static volatile SingularAttribute<Voters, String> motherName;
    public static volatile SingularAttribute<Voters, Date> birthDate;
    public static volatile SingularAttribute<Voters, String> judiciary;
    public static volatile SingularAttribute<Voters, String> religion;
    public static volatile SingularAttribute<Voters, String> firstName;
    public static volatile SingularAttribute<Voters, String> province;
    public static volatile SingularAttribute<Voters, String> listReligion;
    public static volatile SingularAttribute<Voters, String> middleName;
    public static volatile SingularAttribute<Voters, Integer> id;
    public static volatile SingularAttribute<Voters, String> village;

}